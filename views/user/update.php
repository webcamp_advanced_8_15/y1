<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::t('app', 'Update profile: ', [
    'modelClass' => 'User',
]) . ' ' . $model->email;

$this->params['breadcrumbs'][] = Yii::t('app', 'Update profile');
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
