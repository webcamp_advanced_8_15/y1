<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->email;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update profile'), ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <p>
        Date : <?=Yii::$app->formatter->asDateTime(date('Y-m-d H:i:s'),'long');?>
        <br>
        Date other : <?=Yii::$app->formatter->asDateTime(date('Y-m-d H:i:s'),'MM/dd/yyyy');?>
        <br>
        Date no format : <?=Yii::$app->formatter->asDateTime(date('Y-m-d H:i:s').' CEST');?>

        <br>
        Info : <?=Html::encode($model->info);?>
        
        <br>
        Info : <?=Yii::$app->formatter->asDate(null);?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        // 'template' => '{label} -> {value} <br />',
        'attributes' => [
            'fullName',
            'email:email',
            'info',
            'info:html',
            'info:ntext',
            'photo:image',
        ],
    ]) ?>
    <br>
    <h2>Invoice list</h2>
        <?= GridView::widget([
        'dataProvider' => $invoiceDataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'invoiceId',
            'user.fullName',
            'fullName' => [
                'label' => 'User name & Email',
                'value' => function($model) {
                return $model->user->fullName.'('.$model->user->email.')';
            }],
            'date',
            'currency',
        ],
    ]); ?>

</div>
