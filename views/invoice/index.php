<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Invoices');
$this->params['breadcrumbs'][] = $this->title;

$access = true;
?>
<div class="invoice-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= Url::home(); ?> <br>
    <?= Url::base(true); ?> <br>
    <?= Url::base('https'); ?> <br>
    <?= Url::toRoute(['create']); ?> <br>
    <?= Url::toRoute(['user/create']); ?> <br>
    <?= Url::toRoute(['user/update','id' => 123,'type' => 'test']); ?> <br>
    <?= Url::toRoute(['user/update','id' => 123,'type' => 'test']); ?> <br>
    <?= Url::to(['user/update','id' => 123,'type' => 'test'],true);?> <br>



    <p>
        <?= Html::a(Yii::t('app', 'Create Invoice'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        // 'itemView' => function ($model, $key, $index, $widget) {
        //     return Html::a(Html::encode($model->invoiceId), ['view', 'id' => $model->invoiceId]);
        // },
        'itemView' => '_item'
    ]) ?>

    <?= GridView::widget([
        'id' => 'invoice-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn'
            ],
            [
                'class' => 'yii\grid\CheckboxColumn',
                'name' => 'invoiceId[]',
                'checkboxOptions' => function($model) {
                        return [
                                'value' => $model->primaryKey,
                                'checked' => false,
                                'class' => 'checkbox-item',
                            ];
                }
            ],

            'invoiceId',
            // 'user.email:email',
            [
                // 'attribute' => 'user.email'.
                'value' => function ($model) {
                    return $model->user->email;
                },
                'label' => 'User email'
            ],
            'user.fullName',
            'date',
            'currency',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $access ? '{duplicate} {view} {update} {delete}' : '{view}',
                'urlCreator' => function($action, $model, $key, $index) {
                        return ['invoice/'.$action.'/'.$model->primaryKey];

                },
                'buttons' => [
                    'duplicate' => function ($url, $model, $key) {
                    $options = [
                        'title' => Yii::t('yii', 'Duplicate'),
                        'aria-label' => Yii::t('yii', 'Duplicate'),
                        'data-pjax' => '0',
                    ];
                    return Html::a('<span>Duplicate</span>', $url, $options);
                }
                ]
            ],
        ],
    ]); ?>

</div>
