<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\InvoiceSearch;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;


/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['profile','update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionProfile()
    {
        $id = Yii::$app->user->getId();
        $model = $this->findModel($id);

        $searchModel = new InvoiceSearch();
        $searchModel->userId = $id;
        $invoiceDataProvider = $searchModel->search([]);

        return $this->render('profile', [
            'model' => $this->findModel($id),
            'invoiceDataProvider' => $invoiceDataProvider
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdate()
    {
        $id = Yii::$app->user->getId();
        $model = $this->findModel($id);
        $model->setScenario('profile');
        $model->load(Yii::$app->request->post());
        $model->image = UploadedFile::getInstance($model, 'image');
        if ($model->validate()) {
            if($model->image) {
                $photoPath = '/uploads/' . $model->userId.'_'.$model->image->baseName . '.' . $model->image->extension;
                $model->image->saveAs(Yii::getAlias('@webroot').$photoPath);
                $model->photo = $photoPath;
            }
            $model->save(false);
            return $this->redirect(['profile']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
