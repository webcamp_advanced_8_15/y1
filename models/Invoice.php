<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Invoice".
 *
 * @property integer $invoiceId
 * @property integer $userId
 * @property string $date
 * @property string $currency
 *
 * @property User $user
 */
class Invoice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId'], 'integer'],
            [['date'], 'safe'],
            [['currency'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'invoiceId' => Yii::t('app', 'Invoice ID'),
            'userId' => Yii::t('app', 'User ID'),
            'date' => Yii::t('app', 'Date'),
            'currency' => Yii::t('app', 'Currency'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['userId' => 'userId']);
    }
}
