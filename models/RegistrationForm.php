<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * RegistrationForm is the model behind the login form.
 */
class RegistrationForm extends Model
{
    public $email;
    public $password;
    public $passwordConfirm;

    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['password','email','passwordConfirm'], 'required'],
            [['passwordConfirm'], 'compare','compareAttribute' => 'password'],

        ];
    }

    public function find() {
        return User::find();
    }

    public function register()
    {
        if ($this->validate()) {
            $user = new User();
            $user->setAttributes($this->getAttributes());
            $user->setScenario('register');
            if($user->save()) {
                return true;
            } else {
                $this->duplicateErrorsFromModel($user);
            }
        }

        return false;
    }

    protected function duplicateErrorsFromModel(\yii\base\Model $model)
    {
        foreach ($model->getErrors() as $attribute => $errors) {
            // if($this->hasProperty($attribute)) {
                foreach ($errors as $error) {
                    $this->addError($attribute,$error);
                }
            // }
        }
    }
}
