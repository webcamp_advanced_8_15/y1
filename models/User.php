<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "User".
 *
 * @property integer $userId
 * @property string $email
 * @property string $fullName
 * @property string $password
 * @property string $info
 * @property string $authKey
 * @property string $accessToken
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'User';
    }

    /**
     * @var UploadedFile image attribute
     */
    public $image;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required','on' => 'register'],
            [['email','fullName'], 'required','on' => 'profile'],
            [['info'], 'string'],
            [['email'], 'unique'],
            [['email', 'fullName', 'authKey', 'accessToken'], 'string', 'max' => 200],
            [['password'], 'string', 'max' => 20,'min' => 4],
            [['info','authKey','accessToken'], 'default', 'value' => ' '],
            [['fullName'], 'default', 'value' => 'John Doe'],
            [['photo'], 'safe'],
            [['image'], 'file','on' => 'profile'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userId' => Yii::t('app', 'User ID'),
            'email' => Yii::t('app', 'Email'),
            'fullName' => Yii::t('app', 'Full Name'),
            'password' => Yii::t('app', 'Password'),
            'info' => Yii::t('app', 'Info'),
            'authKey' => Yii::t('app', 'Auth Key'),
            'accessToken' => Yii::t('app', 'Access Token'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return User::find()->where(['userId' => $id])->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return User::find()->where(['accessToken' => $token])->one();
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return User::find()->where(['email' => $username])->one();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->userId;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === md5($password);
    }

    public function getFullName() {
        return $this->fullName . ' ('.$this->email.')';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['userId' => 'userId']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if(empty($this->password)) {
                unset($this->password);
            } else {
                // $this->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
                $this->password = md5($this->password);
            }
            return true;
        } else {
            return false;
        }
    }

}
