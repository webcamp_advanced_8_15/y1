<?php

use yii\db\Schema;
use yii\db\Migration;

class m150718_110833_invoice extends Migration
{
    public function up()
    {
        $this->createTable('Invoice', [
            'invoiceId' => Schema::TYPE_PK,
            'userId' => Schema::TYPE_INTEGER,
            'date' => Schema::TYPE_DATE,
            'currency' => Schema::TYPE_STRING,
        ]);
        $this->addForeignKey('User_Invoice_fk','Invoice','userId','User','userId');
    }

    public function down()
    {
        $this->dropForeignKey('User_Invoice_fk','Invoice');
        $this->dropTable('Invoice');
    }

}
