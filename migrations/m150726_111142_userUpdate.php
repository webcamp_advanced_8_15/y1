<?php
use yii\db\Schema;
use yii\db\Migration;

class m150726_111142_userUpdate extends Migration {
    public function up() {
        $this->addcolumn('User', 'photo', Schema::TYPE_STRING . ' Default NULL');
    }
    
    public function down() {
        $this->dropColumn('User', 'photo');
    }
}
